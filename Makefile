#!/usr/bin/make -f
# -*- mode:makefile -*-

help:
	echo "make help       - Imprime esta ayuda"
	echo "make push       - Sube cambios al repositorio"
	echo "make credentials - Guarda las credenciales durante una hora"
	echo "make pull	      - Actualiza el repositorio"

push: 
	git add *
	git commit -m "Subiendo archivos"
	git push origin master

pull:
	git pull

credentials:
	git config --global credential.helper 'cache --timeout 3600'

# git commit --amend -m "an updated commit message" --> cambiar ultimo commit. Mas info en https://www.atlassian.com/git/tutorials/rewriting-history
